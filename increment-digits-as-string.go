package main
import (
	"fmt"
	"log"
	"strconv"
)
func main() {
  arr := []string{"1", "9", "8", "2"}
  incriment(arr)
  fmt.Printf("result: %v", arr)
}

func incriment(input []string) {
	if len(input) != 4 {
		log.Fatal("invalid input")
	}
	
	var plusOneFromChild bool
	
	for i := 3; i >= 0; i-- {
		plusOneFromChild = incrementSingle(&input[i])
		if !plusOneFromChild {
			break
		}
	}
	
}

func incrementSingle(s *string) (plusOneToParent bool) {
	value, _ := strconv.Atoi(*s)
	if value == 9 {
		*s = "0"
		plusOneToParent = true
	} else {
		value++
		*s = strconv.Itoa(value)
	}
	return
} 