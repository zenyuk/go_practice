package main
import "fmt"
func main() {
  arr := []int{1, 10, 0, 2, 8, 3, 0, 0, 6, 4, 0, 5, 7, 0, 0}
  moveZeros(arr)
  fmt.Printf("result: %v", arr)
}

func moveZeros(input []int) {
  write := 0
  for i, v := range input {
    if v != 0 {
      input[write] = input[i]
      write++
    }
  }
  for i := write; i < len(input); i++ {
    input[i] = 0
  }
}